import { RetrieveStationListQuery } from './retrieve-station-list.query';
import { StationAggregateService } from '../../aggregates/station-aggregate/station-aggregate.service';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';

@QueryHandler(RetrieveStationListQuery)
export class RetrieveStationListHandler
  implements IQueryHandler<RetrieveStationListQuery> {
  constructor(private readonly manager: StationAggregateService) {}
  async execute(query: RetrieveStationListQuery) {
    const { offset, limit, search, sort } = query;

    return await this.manager.getStationList(
      Number(offset),
      Number(limit),
      search,
      sort,
    );
  }
}
