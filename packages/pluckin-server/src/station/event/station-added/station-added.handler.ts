import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { StationAddedEvent } from './station-added.event';
import { StationService } from '../../entity/station/station.service';

@EventsHandler(StationAddedEvent)
export class StationAddedHandler implements IEventHandler<StationAddedEvent> {
  constructor(private readonly stationService: StationService) {}
  async handle(event: StationAddedEvent) {
    const { station: stationPayload } = event;
    await this.stationService.create(stationPayload);
  }
}
