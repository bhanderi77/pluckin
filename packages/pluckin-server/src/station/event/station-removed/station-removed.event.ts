import { IEvent } from '@nestjs/cqrs';
import { Station } from '../../entity/station/station.entity';

export class StationRemovedEvent implements IEvent {
  constructor(public station: Station) {}
}
