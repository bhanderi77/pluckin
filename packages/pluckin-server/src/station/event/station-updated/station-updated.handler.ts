import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { StationUpdatedEvent } from './station-updated.event';
import { StationService } from '../../entity/station/station.service';

@EventsHandler(StationUpdatedEvent)
export class StationUpdatedHandler
  implements IEventHandler<StationUpdatedEvent> {
  constructor(private readonly stationService: StationService) {}

  async handle(event: StationUpdatedEvent) {
    const { updatePayload } = event;
    delete updatePayload._id;
    await this.stationService.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}
