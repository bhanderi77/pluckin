import { InjectRepository } from '@nestjs/typeorm';
import { Station } from './station.entity';
import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { MongoRepository } from 'typeorm';
import {
  MONGO_LOCATION_INDEX_TYPE,
  MONGO_LOGGER_CONTEXT,
} from '../../../constants/app-strings';
import {
  MONGO_LOCATION_INDEX_SUCCESSFUL,
  MONGO_LOCATION_INDEX_FAILURE,
} from '../../../constants/messages';

@Injectable()
export class StationService implements OnApplicationBootstrap {
  constructor(
    @InjectRepository(Station)
    private readonly stationRepository: MongoRepository<Station>,
  ) {}

  async find(query?) {
    return await this.stationRepository.find(query);
  }

  async create(station: Station) {
    const stationObject = new Station();
    Object.assign(stationObject, station);
    return await this.stationRepository.insertOne(stationObject);
  }

  async findOne(query, param?) {
    return await this.stationRepository.findOne(query, param);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.stationRepository.manager.connection
      .getMetadata(Station)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.stationRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.stationRepository.count(where),
      offset: skip,
    };
  }

  async deleteOne(query) {
    return await this.stationRepository.deleteOne(query);
  }

  async deleteMany(query, params?) {
    return await this.stationRepository.deleteMany(query, params);
  }

  async update(query, params) {
    return await this.stationRepository.update(query, params);
  }

  async updateOne(query, params) {
    return await this.stationRepository.updateOne(query, params);
  }

  onApplicationBootstrap() {
    return this.stationRepository
      .createCollectionIndex({ location: MONGO_LOCATION_INDEX_TYPE })
      .then(success => {
        Logger.log(MONGO_LOCATION_INDEX_SUCCESSFUL, MONGO_LOGGER_CONTEXT);
      })
      .catch(error => {
        Logger.error(MONGO_LOCATION_INDEX_FAILURE, error, MONGO_LOGGER_CONTEXT);
      });
  }
}
