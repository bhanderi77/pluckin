import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { StationService } from './station.service';
import { Station } from './station.entity';

describe('StationService', () => {
  let service: StationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: StationService,
          useValue: {
            createLocationIndex: (...args) => {},
          },
        },
        {
          provide: getRepositoryToken(Station),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<StationService>(StationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
