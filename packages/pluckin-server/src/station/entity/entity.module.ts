import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Station } from './station/station.entity';
import { StationService } from './station/station.service';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [TypeOrmModule.forFeature([Station]), CqrsModule],
  providers: [StationService],
  exports: [StationService],
})
export class StationEntitiesModule {}
