import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import {
  StationDto,
  MongoGeoLocationType,
} from '../../entity/station/station-dto';
import { Station } from '../../entity/station/station.entity';
import { StationAddedEvent } from '../../event/station-added/station-added.event';
import { StationService } from '../../entity/station/station.service';
import { StationRemovedEvent } from '../../event/station-removed/station-removed.event';
import { StationUpdatedEvent } from '../../event/station-updated/station-updated.event';
import { GetStationByLocationInterface } from '../../entity/station/station-by-location.interface';

@Injectable()
export class StationAggregateService extends AggregateRoot {
  constructor(private readonly stationService: StationService) {
    super();
  }

  addStation(stationPayload: StationDto, clientHttpRequest) {
    const station = new Station();
    Object.assign(station, stationPayload);
    station.uuid = uuidv4();
    this.apply(new StationAddedEvent(station, clientHttpRequest));
  }

  async retrieveStationByLocation(query: GetStationByLocationInterface) {
    return await this.stationService.find({
      location: {
        $near: {
          $geometry: {
            type: MongoGeoLocationType[0],
            coordinates: query.coordinates,
          },
          $maxDistance: query.maxDistance,
          $minDistance: query.minDistance,
        },
      },
    });
  }

  async getStationList(offset, limit, search, sort) {
    return this.stationService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.stationService.findOne(uuid);
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new StationRemovedEvent(found));
  }

  async update(updatePayload: Station) {
    const provider = await this.stationService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new StationUpdatedEvent(update));
  }
}
