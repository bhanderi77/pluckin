import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateStationCommand } from './update-station.command';
import { StationAggregateService } from '../../aggregates/station-aggregate/station-aggregate.service';

@CommandHandler(UpdateStationCommand)
export class UpdateStationHandler
  implements ICommandHandler<UpdateStationCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: StationAggregateService,
  ) {}

  async execute(command: UpdateStationCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.update(updatePayload);
    aggregate.commit();
  }
}
