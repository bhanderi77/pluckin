import { ICommand } from '@nestjs/cqrs';
import { Station } from '../../entity/station/station.entity';

export class UpdateStationCommand implements ICommand {
  constructor(public readonly updatePayload: Station) {}
}
