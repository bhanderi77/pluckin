import { AddStationHandler } from './add-station/add-station.handler';
import { RemoveStationHandler } from './remove-station/remove-station.handler';
import { UpdateStationHandler } from './update-station/update-station.handler';

export const StationCommandManager = [
  AddStationHandler,
  RemoveStationHandler,
  UpdateStationHandler,
];
